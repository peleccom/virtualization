from django import forms

from gallery.models import GalleryItem


class GalleryItemUploadForm(forms.ModelForm):
    class Meta:
        model = GalleryItem
        fields = (
            'image',
            'name',
        )
