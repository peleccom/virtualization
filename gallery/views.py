# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django.urls import reverse
from django.views.generic import TemplateView, CreateView, ListView, DetailView

from gallery.forms import GalleryItemUploadForm
from gallery.models import GalleryItem
from gallery.utils import find_similar_images


class Gallery(ListView):
    template_name = "gallery/gallery.html"
    model = GalleryItem

    def get_queryset(self):
        qs = super(Gallery, self).get_queryset()
        qs = qs.order_by('-created')
        return qs


class GalleryPhotoUploadView(CreateView):
    form_class = GalleryItemUploadForm
    template_name = 'gallery/upload.html'

    def get_success_url(self):
        return reverse('gallery:gallery-list')


class GalleryItemView(DetailView):
    template_name = "gallery/image.html"
    model = GalleryItem

    def get_context_data(self, **kwargs):
        ctx = super(GalleryItemView, self).get_context_data(**kwargs)
        ctx['similar_items'] = find_similar_images(self.get_object())
        return ctx