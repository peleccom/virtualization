import boto3


def detect_labels(file, max_labels=10, min_confidence=60, region="eu-west-1"):
    rekognition = boto3.client("rekognition", region)
    response = rekognition.detect_labels(
        # Image={
        #     "S3Object": {
        #         "Bucket": bucket,
        #         "Name": key,
        #     }
        # },
        Image={
            "Bytes": file.read()
        },
        MaxLabels=max_labels,
        MinConfidence=min_confidence,
    )
    return response['Labels']


def similarity(gallery_item1, gallery_item2):
    item1_tags = gallery_item1.item_tags.all()
    item2_tags = gallery_item2.item_tags.all()
    item1_map = {}
    item2_map = {}
    for item1_tag in item1_tags:
        item1_map[item1_tag.tag.id] = item1_tag.confidence
    for item2_tag in item2_tags:
        item2_map[item2_tag.tag.id] = item2_tag.confidence
    for item1_tag_id in item1_map.keys():
        if item1_tag_id not in item2_map:
            item2_map[item1_tag_id] = 0
    for item2_tag_id in item2_map.keys():
        if item2_tag_id not in item1_map:
            item1_map[item2_tag_id] = 0
    tag_ids = item1_map.keys()
    min_sum = max_sum = 0
    for tag_id in tag_ids:
        min_sum += min(item1_map[tag_id], item2_map[tag_id])
        max_sum += max(item1_map[tag_id], item2_map[tag_id])
    return float(min_sum) / max_sum

# def similarity(gallery_item1, gallery_item2):
#     item1_tags = gallery_item1.item_tags.all()
#     item2_tags = gallery_item2.item_tags.all()
#     item1_map = {}
#     item2_map = {}
#     for item1_tag in item1_tags:
#         item1_map[item1_tag.tag.id] = item1_tag.confidence
#     for item2_tag in item2_tags:
#         item2_map[item2_tag.tag.id] = item2_tag.confidence
#     for item1_tag_id in item1_map.keys():
#         if item1_tag_id not in item2_map:
#             item2_map[item1_tag_id] = 0
#     for item2_tag_id in item2_map.keys():
#         if item2_tag_id not in item1_map:
#             item1_map[item2_tag_id] = 0
#     tag_ids = item1_map.keys()
#     sum_vlaue = 0
#     count = 0
#     for tag_id in tag_ids:
#         v =  (item1_map[tag_id] /100) * (item2_map[tag_id] /100) * (1 - abs(item1_map[tag_id] - item2_map[tag_id])/100)
#         print(v)
#         print(item1_map)
#         print(tag_id)
#         print(gallery_item1.id)
#         print(gallery_item2.id)
#         sum_vlaue += v
#     sum_vlaue /= len(tag_ids)
#     print(sum_vlaue)
#     return sum_vlaue



def find_similar_images(gallery_item, threshold=0.1):
    from gallery.models import GalleryItem
    similarity_values = [
        (similarity(gallery_item, gallery_item2), gallery_item2) for gallery_item2 in GalleryItem.objects.all().order_by("id")
        if gallery_item.pk != gallery_item2.pk
    ]
    similarity_values = [(similarity_value, item) for (similarity_value, item) in similarity_values if similarity_value > threshold]
    similarity_values.sort(key=lambda x: x[0], reverse=True)
    return similarity_values[:10]