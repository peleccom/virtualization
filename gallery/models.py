# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.six import python_2_unicode_compatible
# Create your models here.
from gallery.utils import detect_labels


@python_2_unicode_compatible
class GalleryItem(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255, blank=True)
    image = models.ImageField()
    tags = models.ManyToManyField("GalleryTag", through="ItemTag")


    def save(self, *args, **kwargs):
        super(GalleryItem, self).save(*args, **kwargs)
        labels = detect_labels(self.image.file)
        for label in labels:
            name = label['Name']
            confidence = label['Confidence']
            gallery_tag, created = GalleryTag.objects.get_or_create(name=name)
            ItemTag.objects.create(item=self, tag=gallery_tag, confidence=confidence)

    def __str__(self):
        return "{} - {}".format(self.name, self.image)


@python_2_unicode_compatible
class ItemTag(models.Model):
    item = models.ForeignKey("GalleryItem", related_name="item_tags")
    tag = models.ForeignKey("GalleryTag")
    confidence = models.FloatField()

    def __str__(self):
        return "{} - {} - {}".format(self.item, self.tag, self.confidence)


@python_2_unicode_compatible
class GalleryTag(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return "{}".format(self.name)