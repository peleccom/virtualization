from __future__ import absolute_import
from django.contrib import admin

# Register your models here.
from . import models

admin.site.register(models.GalleryItem)
admin.site.register(models.GalleryTag)
admin.site.register(models.ItemTag)
