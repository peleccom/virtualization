import django.apps


class GalleryConfig(django.apps.AppConfig):
    name = 'gallery'
